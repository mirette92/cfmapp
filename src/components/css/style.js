
import {Platform,StyleSheet,  Dimensions} from 'react-native';


const styles = StyleSheet.create({
    container:{
        flex : 1,
        justifyContent: 'center',
        alignItems: 'center',
      
       
        flexDirection: 'column',
    },
    underline: {textDecorationLine: 'underline'},

    contentCenter:{
      justifyContent:'center',
      alignItems:'center',
    },
    fontSize:{
      fontSize:15,
      color:'#3e3d40',
    },
    blackColor:{
      color:'#3e3d40',
    },
    redColor:{
      color:'#e2001a',
    },
    grayColor:{
        color:'#5e5e5e',
    },
    lightGray:{
        color:'#E2E2E2',
    },
    whiteColor:{
        color:"#fff",
    },
    itemOverrideStyle:{
      borderBottomColor:'transparent',
    },
    buttonSubmitDefaultStyle:{
      backgroundColor:'#e2001a',
      width:'90%',
      justifyContent:'center',
      marginTop:'5%',
    },
    buttonSubmitWhiteDefaultStyle:{
      backgroundColor:'#fff',
      width:'90%',
      justifyContent:'center',
      marginTop:'5%',
      borderWidth:1,
      borderColor:'#e2001a'
    },
    ItemForm:{
      width:'90%',
      justifyContent:'center',
     
    },
    QuestionaireForm:{
        width:'90%',
    },
    inline:{
      flexDirection:'row',
      justifyContent:'center'
    },
    spaceAround:{
        justifyContent:'space-around',
    },
    paddingTop:{
        paddingTop:'5%',
    },
    backgroundTransparent:{
        backgroundColor:'transparent',
    },
    fontSize8:{
        fontSize:8,
    },
    fontSize12:{
        fontSize:12,
    },
    fontSize14:{
      fontSize:14,
    },
    fontSize18:{
        fontSize:18,
    },
    bold:{
        fontWeight:'bold'
    },
    activeBtn:{
        backgroundColor: '#e2001a',
        color:'#fff'
    },
    unactiveBtn:{
        backgroundColor: '#fff',
        borderWidth:1,
        borderColor: '#e2001a',
        color:'#e2001a'
    },
    padding_lt:{
        paddingLeft:'4%',
    },
    padding_rt:{
        paddingRight:'4%',
    },
    backgroungImgView:{
        width: '100%', 
        height: '40%',
        
    },
    backgroungImgViewOrder:{
        width: '100%', 
        height: '30%',
        
    },
    image:{
        width: '100%', 
        height: '100%'
    },
    logo:{
        resizeMode:'center',
        width:'60%',
        height:'40%'
    },
    iconNotification:{
        
        right: 50
    },
    iconChat:{
        
        right: 10
    },
    paddingTop10:{
        paddingTop:'10%',
    },
    marginBottom:{
        marginBottom:'20%'
    },
    fontSize25:{
        fontSize:25
    },
    viewSegment:{
        width:'100%',
        flexWrap:'wrap'
    },
    viewWrap:{
      flexWrap:'wrap'
    },
    viewSegmentBtn:{
        backgroundColor:'#fff',
        flexDirection:'row',
        justifyContent:'space-between'
    },
    unactivetextSegmentbtn:{
        color:'#5e5e5e',
        fontSize:12,
        paddingLeft:0,
    },
    activetextSegmentbtn:{
        color:'#e2001a',
        fontSize:14,
        paddingLeft:0,
    },
    segmentbtn:{
        flexGrow:1,
        borderWidth:1,
        borderTopWidth:3,
        
        borderRadius:0
    },
    iconSegment_lt:{
        
        paddingLeft:'2%'
    },
    iconSegment_rt:{
        
        paddingRight:'2%'
    },
    
    iconTimerSegment_lt:{
       
        paddingLeft:'1%'
    },
    iconTimerSegment_rt:{
       
        paddingRight:'1%'
    },
    container: {
        display: 'flex',
        flex:1
    },
    activeSegment:{
        borderColor:'#5e5e5e',
    },
    unactiveSegment:{
        borderColor:'#E2E2E2',
    },
    textCard_lt:{
        marginLeft:'5%'
    },
    textCard_rt:{
        marginRight:'5%'
    },
    backgroundScrollColor:{

        backgroundColor:'#E2E2E2',
    },
    dropDown:{
        width: '90%' ,
        marginLeft:'5%',
        fontSize:15,
    },
    compNameStyle:{
        fontWeight:'bold',
        fontSize:18,
        color:'#fff',
        paddingLeft:'4%',
        paddingTop:'2%'
      },
      workNumberStyle:{
    
        fontSize:10,
        color:'#fff',
        paddingLeft:'11%',
      },
      quesTextStyle:{
        fontSize:16,
        color:'#e5322d',
        marginLeft:'5%',
        paddingTop:'4%',
      },
      
      iconRing:{
        flexDirection:'row',
        paddingLeft:'5%',
    
      },
      background:{
        width: '100%',
        backgroundColor: '#e5322d',
      },
      flowDirectionColView:{
        flexDirection:'column',
      },
      widthUndefined:{
          width:undefined,
      },
      bottomBorder0:{
        borderBottomWidth:0,
      },
      bottomBorder0Color:{
        borderBottomWidth:0,
        borderBottomColor:'#fff'
      },
      TextStyle_lt: {
        marginLeft:'3%'
      },
      TextStyle_rt: {
        marginLeft:'3%'
      },
      TextStyleVersion: {
        textAlign:'center',
      },
      whiteBackgroundColor:{
        backgroundColor:'#fff'
     },
      ImgTextInput:{
        marginTop:'1%',
        flexDirection:'row',
        justifyContent:'space-evenly',
      },
      OfferImg:{
        height:'100%',
        width:'35%',
        backgroundColor:'#E2E2E2'
      },
      offerCard:{
        flexDirection:'row',
        justifyContent:'space-around'
      },
      empolyeeView:{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center" ,
      },
      employeeViewName:{
        flexDirection: "row",
        justifyContent: "space-between",
      },
      textStyleEmployee:{
        backgroundColor: "#E2E2E2",
        padding: 10,

        fontSize:14
      },
      paddingMarginTextarea:{
        width:'70%',
        borderRadius:10,
        borderWidth: 1,
        borderColor:'#E2E2E2',
        backgroundColor:'#fff',
    
      },
      row: {
        padding: 4,
        paddingLeft: 0,
      },
      content: {
        marginLeft: 40,
      },
      timeline: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        width: 40,
        justifyContent: 'center', // center the dot
        alignItems: 'center',
      },
      line: {
        position: 'absolute',
        top: 0,
        left: 18,
        width: 4,
        bottom: 0,
      },
      topLine: {
        flex: 1,
        width: 4,
        backgroundColor: '#5e5e5e',
      },
      bottomLine: {
        flex: 1,
        width: 4,
        backgroundColor: '#5e5e5e',
      },
      hiddenLine: {
        width: 0,
      },
      dotActive: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: 'green',
      },
      dotunActive: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#e2001a',
      },
      CardProfile: {
        flexDirection:'column',
    
      },
      TextStyleVersion: {
        textAlign:'center',
      },
      title:{
        fontSize:12,color:'#c2c2c2',paddingTop:'5%'
      },
      titleOffice:{
        fontSize:15,color:'#c2c2c2',paddingTop:'2%'
      },
      answer:{
        fontSize:12,paddingLeft:'4%',color:'#252525',borderBottomWidth:1
      },
      selfAlign:{
        alignSelf: "center"
      },
      padding:{
        padding:10,
      },
      paddingBottom:{
       ...Platform.select({ ios: { paddingBottom: 25 }, android: {textAlignVertical:'center'} }),

      },
      flexGrow:{
        flexGrow:3,
      },
      header:{
        backgroundColor: "#5e5e5e",
      },
      headerContent:{
        padding:30,
        alignItems: 'center',
      },
      avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom:10,
      },
      name:{
        fontSize:22,
        color:"#FFFFFF",
        fontWeight:'600',
      },
      bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding:30,
      },
      textInfo:{
        fontSize:18,
        marginTop:20,
        color: "#696969",
      },
      right:{
        alignItems:'flex-end',
      },
      iconSend:{
        width:30,
        height:30,
        alignSelf:'center',
      },
      ImgOffer:{
        width:70,
        height:80,
        alignSelf:'center',
      },
      containerC: {
        flex: 1,
        height:'100%'
      },
      preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height-70,
        width: Dimensions.get('window').width
      },
      capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
      }
  });

  export default styles;