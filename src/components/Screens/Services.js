
import React ,{Component} from 'react';
import {View,AsyncStorage,Image} from 'react-native';
import {Content, Text,CardItem,Card,Spinner} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';
import styles from '../css/style';
import * as axios from 'axios';

class Services extends React.Component{

    state = {
      arrService: [],
      ShowSpinner:true
    }

    addWorkOrder(index){
      global.service = this.state.arrService[index]['id']
   
      this.props.navigation.navigate('AddOrder');
    }
    listService(){
      console.log('hii');
      axios.get('http://contrackfm.bit68.com/api/service/').then(response=> {
       
        const arrService = response.data;
        this.setState({ShowSpinner:false});
        this.setState({arrService:arrService})
        console.log(arrService);
        
      })
      console.log(global.token)
      console.log('hii');
    }
    componentDidMount(){
     this.listService();
    }
    
    render()
    {
      
      return(
        <Content style={styles.backgroundScrollColor}>
   
   {this.state.ShowSpinner &&
          <View style={styles.spaceAround}>
            <Spinner  color='red' />
          </View>
        }
        
          {this.state.arrService.map((Service,key)=>
            <Card key={key} >
              <CardItem header button onPress={(event)=>this.addWorkOrder(key)}>
                <Image source={{uri:Service.icon}} style={styles.iconSend}  />
                
                <View style={{flexWrap:'wrap'}}>
                <Text uppercase={false} style={styles.textCard_lt}>{Service.name}</Text>
            
                <Text uppercase={false} style={[styles.textCard_lt,styles.fontSize12,styles.grayColor]}>{Service.description}</Text>
                </View>
              </CardItem>
    
            </Card>
           
           )}
        </Content>
      )
    }

}

export default withNavigation(Services);
