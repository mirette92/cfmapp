import React ,{Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import {Content,Card,Button,CardItem, Spinner,Accordion,Title,Icon} from 'native-base';
import { withNavigation } from 'react-navigation';
import styles from '../css/style';
import {AsyncStorage} from 'react-native';
import * as axios from 'axios';

const WOArray = [];
WOArray[0] = { title: "Open UnSchedule", };
WOArray[1] = { title: "Open Schedule", };
WOArray[2] = { title: "open Inprogress",};
WOArray[3] = { title: "open Completed",};





class Current extends Component{
  constructor(props) {
    super(props);
    this._renderHeader = this._renderHeader.bind(this);
    this._renderContent = this._renderContent.bind(this);
  }
  state = {
    arrWorkOrder: [],
    ShowSpinner:true,
    user:''
  }
  listWorkOrder=async()=>{
    let data = {
      
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    
    axios.get('http://contrackfm.bit68.com/api/work_order?status=1&status1=2&status2=3&status3=4',data).then(response=> {

      const arrWorkOrder = response.data.arrWorkOrder;
      this.setState({ShowSpinner:false});
      console.log(arrWorkOrder);
      this.setState({arrWorkOrder:arrWorkOrder})
      
      
    }).catch(function(error){
      console.log('there is'+ error.message)
    })
  } 
  componentDidMount(){
    this.listWorkOrder();
    
   } 
  _renderHeader(item, expanded) {
    return (
      <View style={[styles.empolyeeView,styles.whiteBackgroundColor]}>
          <View style={styles.employeeViewName}>
            
            <Text style={styles.padding_lt}> {item.cfm_wo_code}.</Text>
            
          </View>
        {expanded
          ? <Icon style={styles.fontSize18} name="remove" />
          : <Icon style={styles.fontSize18} name="add" />}
      </View>
    );
  }
  _renderContent(item) {
    const x = 1;
    return (
      <View >
        
        {WOArray.map((prop, key) => {
         
         const total = WOArray.length;
         const topLineStyle = key == 0 ? [styles.topLine, styles.hiddenLine] : styles.topLine;
         const bottomLineStyle = key == total - 1 ? [styles.bottomLine, styles.hiddenLine] : styles.bottomLine;
         
         return(
        <View key={key} >
        
         
         
          <View style={styles.row}>
            <View style={styles.timeline}>
              <View style={styles.line}>
                <View style={topLineStyle} />
                <View style={bottomLineStyle} />
              </View>
              
                <View style={item.status >= key+1 ? styles.dotActive : styles.dotunActive}  >
                
              </View>
            </View>
            <Card style={styles.content}>
              <CardItem style={{flexDirection: 'column', alignItems: 'flex-start', flexWrap: 'wrap'}} >
           
                <Text style={[styles.grayColor,styles.bold,styles.fontSize18]}>{prop.title} </Text>
                {key == 0 ?
                <Text>user :{item.user.name}  </Text> :null }
                {key == 0 ?
                <Text>Job :{item.service.name}  </Text> :null }
                {key == 0 ?
                <Text>Problem :{item.problem.name}  </Text>:null }
                {key == 0 ?
                <Text>External Project :{item.external_project.name}</Text>:null
                }
                 {key == 0 ?
                <Text>building :{item.building.name}</Text>:null
                }
                 {key == 0 ?
                <Text>floor :{item.floor.name}</Text>:null
                }
              {prop.title  == 'Done'? 
                <Button transparent onPress={() => this.props.navigation.navigate('ConfirmWorkOrder')} >
                <Text style={[styles.redColor,styles.underline]}>Rate Service</Text>
                </Button>
                :null}
              </CardItem> 
            </Card>
          </View>
       
        </View>
         );
         
        
           
          })}
      </View>
    );
  }


  render() {
    return (
      <Content style={styles.backgroundScrollColor}>
         {this.state.ShowSpinner &&
          <View style={styles.spaceAround}>
            <Spinner  color='red' />
          </View>
        } 
        {this.state.arrWorkOrder.length == 0 &&  this.state.ShowSpinner == false ?
         <View style={{justifyContent:'center', alignContent:'center'}}>
         <Image
         source={require('../../images/no.png')}
         style={{ width: 200, height: 200,justifyContent:'center'}}/>
        
        </View>
        :
       
        <Accordion dataArray={this.state.arrWorkOrder} renderHeader={this._renderHeader}
        renderContent={this._renderContent} expanded={0}/>
      
      
        }
        
       
      </Content>
     
    );
  }

}



export default withNavigation(Current);
