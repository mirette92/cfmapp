//imports
import React ,{Component} from 'react';

import {StyleSheet,View,TouchableHighlight, ImageBackground,TextInput, Image ,TouchableOpacity, ScrollView} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Body,Title,Button,Tab,Tabs,Segment,Card, CardItem} from 'native-base';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { withNavigation } from 'react-navigation';
import styles from '../css/style';
import Services from './Services'
import Completed from './Completed'
import Current from './Current'
import MyStatusBarCustom from '../../helpers/MyStatusBar';
//body(class-[functions-methodes-jsx])

class OrderScreen extends Component{
  constructor () {
    super()
    this.state = {
      activePage: 1,
    }
    this.selectComponent = this.selectComponent.bind(this);

  }
  selectionOnPress(segment,userType) {
    this.setState({ activePage:segment,selectedButton: userType });
    //this.selectComponent(segment);
  }
  selectComponent = (activePage) => () => this.setState({activePage:activePage})

  _renderComponent = () => {
    if(this.state.activePage === 1)
      return <Services/> //... Your Component 1 to display
    else if(this.state.activePage === 2)
      return <Current/>
    else
     return <Completed/> //... Your Component 2 to display
  }



  render(){

    return(
    <View style={styles.container}>
    
        <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" noShadow style={{display:'none'}}/>
        
      <ImageBackground style={[styles.backgroungImgViewOrder]} imageStyle={styles.image}  source={{uri:'https://www.contrackfm.com/wp-content/uploads/2018/02/home-background.jpg'}}>
      <View  style={{flexDirection:'row'}}>
        <Text style={[styles.fontSize25,styles.whiteColor,styles.marginBottom,styles.fontWeight,styles.flexGrow,styles.padding]}>  How can we {"\n"} {" "}Help you today? </Text>
  
        
          <Ionicons size={30} onPress={() => this.props.navigation.navigate('Notification')} style={[styles.whiteColor,styles.padding]} name="ios-notifications" />
    
          <Ionicons size={30} style={[styles.whiteColor,styles.padding]} name="ios-chatboxes" onPress={() => this.props.navigation.navigate('Chat')} />
        </View>
         
      </ImageBackground>
 
      <View style={styles.viewSegment}>
        <View style={styles.viewSegmentBtn}>
          <Button transparent light style={[styles.segmentbtn, styles.unactiveSegment]} first active={this.state.activePage === 1} onPress={this.selectComponent(1)}>
            <Ionicons size={20} style={this.state.activePage === 1 ? [styles.redColor,styles.fontSize25,styles.iconSegment_lt,styles.bold]:[styles.grayColor,styles.iconSegment_lt]} name="ios-list" />
            <Text uppercase={false} style={this.state.activePage === 1 ? [styles.activetextSegmentbtn,styles.activeSegment] : [ styles.unactivetextSegmentbtn , styles.unactiveSegment]}>New WO</Text>
          </Button>
          <Button transparent light style={[styles.segmentbtn,styles.unactiveSegment]} active={this.state.activePage === 2} onPress={this.selectComponent(2)}>
            <MaterialCommunityIcons size={20} style={this.state.activePage === 2 ? [styles.redColor,styles.fontSize25,styles.iconSegment_lt,styles.bold]:[styles.grayColor,styles.iconSegment_lt]} name="timer-sand" />
            <Text uppercase={false} style={this.state.activePage === 2 ? [styles.activetextSegmentbtn,styles.activeSegment] : [ styles.unactivetextSegmentbtn , styles.unactiveSegment]}>Inprogress WO</Text>
          </Button>
          <Button transparent light style={[styles.segmentbtn, styles.unactiveSegment]} last active={this.state.activePage === 3} onPress={this.selectComponent(3)}>
            <Ionicons size={20} style={this.state.activePage === 3 ? [styles.redColor,styles.fontSize25,styles.iconSegment_lt,styles.bold]:[styles.grayColor,styles.iconSegment_lt]} name="ios-timer" />
            <Text uppercase={false} style={this.state.activePage === 3 ?  [styles.activetextSegmentbtn,styles.activeSegment]: [ styles.unactivetextSegmentbtn , styles.unactiveSegment]}>Completed WO</Text>
          </Button>
        </View>
      </View>
      <View style={styles.container}>
      {this._renderComponent()}
      </View>
    </View>

    )
  }
}




//exports
export default withNavigation(OrderScreen);
