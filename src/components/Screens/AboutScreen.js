//imports
import React ,{Component} from 'react';

import {StyleSheet, View,ImageBackground,TextInput, Image ,TouchableOpacity, ScrollView} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Body,Title,Button,Card} from 'native-base';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MyStatusBarCustom from '../../helpers/MyStatusBar';
import styles from '../css/style';

//body(class-[functions-methodes-jsx])
class AboutScreen extends Component{
  onPressLogin = (event)=>{
    alert('you logged successfully');
  }

  render(){
    return(
    <Container style={styles.container}>
      <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d',borderBottomWidth:0}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title style={styles.whiteColor}>About</Title>
        </Body>
        <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
          </Button>
          <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>
        </Right>
      </Header>
      <ImageBackground style={[styles.backgroungImgView,styles.contentCenter]} imageStyle={styles.image}  source={require('../../images/home-background.jpg')}>
        <Image style={styles.logo} source={require('../../images/CFM-logo-white.png')}/>
      </ImageBackground>
      <Content  >
        <Card transparent style={[styles.paddingTop]}>
          <Text style={[styles.fontSize,styles.redColor]}>About</Text>
          <View>
          <Text>Pests are not going to be a problem anymore with CFM’s services. Pest control services are provided under strict controls pertaining to the health and safety of the building’s occupants, and limiting the impact on the environmental, as much as possible. All our chemicals are environmentally friendly and affect neither the surface nor the inhabitants.</Text></View>
        </Card>
      </Content>
    </Container>
    )
  }
}


//css (styling)


//exports
export default AboutScreen;
