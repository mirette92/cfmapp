//imports
import React, {Component} from 'react';


import {Container,Header,Content,Form,Item,Input,Button,Text, Card, Footer,Spinner,Toast, Root} from 'native-base';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../css/style'
import { withNavigation } from 'react-navigation';
import config from './config'
import {AsyncStorage} from 'react-native';
//body(jsx)
class SignupScreen extends Component{
  constructor(props) {
      super(props);
      const { navigate } = this.props.navigation;
      this.state = { account_type: 'User' ,email:'',password:'',mobile:'',name:'',spinner:false,disable:false,showToast:false,message:''};

      this.selectionOnPress = this.selectionOnPress.bind(this);
  }

  
  toasterfunc(message,type){
    Toast.show({
      text: message,
      buttonText: "Got it",
      position:'bottom',
      type:type
    })
  }
  handleClick(event){
   
    this.setState({spinner:true,disable:true})
  
    console.log(this.state)

    //const user = {username:this.state.username, password:this.state.password}

    

    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify({
        email:this.state.email,
        password:this.state.password,
        account_type:this.state.account_type,
        mobile:this.state.mobile,
        name:this.state.name,


      }),
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        
      }
    }
    return fetch('http://contrackfm.bit68.com/api/profile/', data)
            .then(response => response.json())  // promise
            .then(async(json) =>{
              
              if(json.status == 200){
                this.setState({spinner:false,disable:false,showToast:true,message:'account created successfully'})
                this.toasterfunc('Account created successfully!','success')
                await AsyncStorage.setItem('token', json.token);
                
                this.props.navigation.navigate('Order');
                
              }else{
                this.toasterfunc('Failed to create account!','danger')
                this.setState({spinner:false,disable:false,showToast:true,message:'failed to create account'})
                
              }
      
            } )
            
    }
  render(){

    return (
      <Root>
      <Container style={styles.container}>
      <Header androidStatusBarColor="#952421" style={{display:'none'}}/>
      <Content >
        <Form style={styles.paddingTop}>
        
       
          <Item style={[styles.ItemForm,styles.paddingTop]}>
          
          <FontAwesome name="user" color="#e5322d" size={15} />
          <Input onChangeText={(name) => this.setState({name})} placeholder="Enter Full Name" style={[styles.fontSize,styles.blackColor]} />
          </Item>
          <Item style={[styles.ItemForm,styles.paddingTop]}>
          
          <Ionicons name="ios-mail" color="#e5322d" size={15} />
          <Input onChangeText={(email) => this.setState({email})} placeholder="Enter Email" style={[styles.fontSize,styles.blackColor]} />
          </Item>
          <Item style={[styles.ItemForm,styles.paddingTop]}>
            
            <FontAwesome name="mobile" color="#e5322d" size={15}  />
            <Input onChangeText={(mobile) => this.setState({mobile})} placeholder="Enter Mobile" style={[styles.fontSize,styles.blackColor]} />
          </Item>
         
          <Item style={[styles.ItemForm,styles.paddingTop]}>
            <Ionicons name="ios-key" color="#e5322d" size={15} />
            <Input onChangeText={(password) => this.setState({password})} placeholder="Enter Password" style={[styles.fontSize,styles.blackColor]}  secureTextEntry={true}/>
          </Item>
          <Item style={[styles.itemOverrideStyle,styles.paddingTop,,styles.contentCenter]}> 
            <Button disabled={this.state.disable} block  rounded style={styles.buttonSubmitDefaultStyle} onPress={(event) => this.handleClick(event)}>
              {this.state.spinner ? <Spinner  color='#E2E2E2' />:<Text uppercase={false}>Update</Text> }
              
            </Button>
          </Item>
        
        </Form>
       
      </Content>
      <Footer style={styles.backgroundTransparent}>
        
      </Footer>
     
    </Container>  
    </Root>
    );

  }
}

export default withNavigation(SignupScreen) ;

