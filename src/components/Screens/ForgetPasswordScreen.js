import React ,{Component} from 'react';

import {Container,Header,Content,Form,Item,Input,Button,Text} from 'native-base';
import { View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
//css import
import styles from '../css/style';

class ForgetPasswordScreen extends Component{
render(){
  return (
    <Container style={styles.container}>
      <Header androidStatusBarColor="#952421" style={{display:'none'}}/>
      <View style={[styles.contentCenter]}>
          <Text style={[styles.grayColor,styles.fontSize12,styles.paddingTop]}>Enter your register email address {"\n"} </Text>
          <Text style={[styles.grayColor,styles.fontSize12]}>we'll send password reset info on mail</Text>
        </View>
      <Content >
      <Form >
        <Item style={[styles.ItemForm,styles.paddingTop]}>
        
          <FontAwesome name="user" color="#e5322d" size={15} />
          <Input placeholder="Enter Username" style={[styles.fontSize,styles.blackColor,styles.padding_lt]} />
        </Item>
        
  
        <Item style={[styles.itemOverrideStyle,styles.paddingTop]}> 
          <Button block  rounded style={styles.buttonSubmitDefaultStyle}>
            <Text uppercase={false}>Submit</Text>
          </Button>
        </Item>
      </Form>
      
    </Content>
  </Container>
  )
}

}


export default ForgetPasswordScreen;
