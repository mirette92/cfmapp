import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Container, Header, Content, List, ListItem,  Icon, Right,Body,Title,Left,Button,Card,CardItem,Item,Spinner} from 'native-base';
import MyStatusBarCustom from '../../helpers/MyStatusBar';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {AsyncStorage} from 'react-native';



class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "type": "message",
      "from": {
        "id": "user_id",
        "account": "user_account_name",
        "name": "user_name",
        "lang": "en"
      },
      ShowSpinner:"true",
      "text": "",
      "message":"",
      "actions":[],
      data: []
    }
  }
  getUser= async()=>{
    let data = {
      method: 'GET',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    return fetch('http://contrackfm.bit68.com/api/profile/',data)
            .then(response => response.json())  // promise
            .then(json =>{
              
              if(json.status == 200){
                
                this.setState({'company_name':json.data[0].account[0].acount_name})
                global.account_name = json.data[0].account[0].acount_name;
                global.user = json.data
                this.startConversation()
                
              }else{
                
              }

            } )
    
  }

  startConversation= async()=>{
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer 5UtIXSUDOYM.rGLSDcCxyt2h62HgpTqiae8X9dCWkMfTQmqKVznQNUE'
        
      }
    }
    
    return fetch('https://directline.botframework.com/v3/directline/conversations',data)
      .then(response => response.json())  // promise
      .then(async(json) =>{
        global.conversationId =  json.conversationId
        console.log('startCons.'+global.conversationId )
        global.token_chatbot = json.token
        this.startChat()
  
      })
      .catch(error => {
        console.error(error);
        
      });
    
  }

  startChat(){
    var index = global.messageIndex+1
    global.messageIndex = index
    const conversationId = global.conversationId
    console.log('startChat'+conversationId)
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify({
        "type": "conversationUpdate",
        "from": {
          "id": global.user[0].id,
          
        },
        "account": global.account_name,
        "name": global.user[0].name,
        "lang": "en", 
      }),
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ global.token_chatbot
        
      }
    }
  
   
    return fetch('https://directline.botframework.com/v3/directline/conversations/'+conversationId+'/activities', data)
      .then(response => response.json())  // promise
      .then(async(json) =>{
    
        const id = json.id
        const mark = id.split("|")
        global.waterMark = mark[1]
       
       
      } )
      
  }

  
  sendMessageChoice(message){
   this.state.message = message
    this.sendMessage()
  }
  scrollToEnd = async() => {
    const wait = new Promise((resolve) => setTimeout(resolve, 1000));
    wait.then( () => {
        this.flatList.scrollToEnd({ animated: true });
    });
}
  sendMessage = async()=>{
    //this.refs.ListView_Reference.scrollToEnd({animated: true});
 
    var index = global.messageIndex+1
    global.messageIndex = index

    this.setState({ data: [...this.state.data, {id:index,type:'out',date:new Date().getHours() + ":"+ new Date().getMinutes(),message:this.state.message}] }) 
    
    
    this.setState(previousState => ({
      actions: []
    }));
    this.scrollToEnd()
    //this.flatList()
    const conversationId = global.conversationId
 
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify({
        "type": "message",
        "from": {
          "id": global.user[0].id,
          
        },
      
         "text":this.state.message,
        
      }),
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ global.token_chatbot
        
      }
    }
  

  
    this.setState({"message":''})
    return fetch('https://directline.botframework.com/v3/directline/conversations/'+conversationId+'/activities', data)
      .then(response => response.json())  // promise
      .then(async(json) =>{
        global.ChoiceMesage = undefined
        const id = json.id
        const mark = id.split("|")
        global.waterMark = mark[1]
       console.log('sendMessage'+ global.conversationId )
       console.log('watermark'+ global.waterMark)
       
       
      } )
  
  }
  
  receiveMessage(){
    
    
    const conversationId = global.conversationId;
    const mark = global.waterMark
    let data = {
      method: 'GET',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ global.token_chatbot
        
      }
    }
    if(global.waterMark !== 0){
      
      return fetch('https://directline.botframework.com/v3/directline/conversations/'+conversationId +'/activities?watermark='+mark,data)
      .then(response => response.json())  // promise
      .then(async(json) =>{
        //this.refs.ListView_Reference.scrollToEnd({animated: true});
        const data = json
        console.log(json.activities)
        console.log('watermark'+global.waterMark)
        global.waterMark = 0;
        var index1 = global.messageIndex+1
        global.messageIndex = index1
        this.setState({ShowSpinner:false});
        if(json.activities.length > 0){
          Object.keys(json.activities).forEach(function(key, index) {
            
            this.setState(((prevState) => {
              if(json.activities[key].text !== undefined){
              return {
                
                  data: [...prevState.data,{id:index1,type:'in',date:new Date().getHours() + ":"+ new Date().getMinutes(),message:json.activities[key].text}],
                  
                }
               
              }
            }
            
            )) 
            
            if(json.activities[key].suggestedActions !=undefined){
              //this.refs.ListView_Reference.scrollToEnd({animated: true},this);
              this.setState(previousState => ({
                actions: [...previousState.actions, json.activities[key].suggestedActions.actions]
              }));
            }
            
            index1++
            global.messageIndex = index1
            
          }, this)
     
        
        }
        this.scrollToEnd()
      })
      
    
      
    }
  }
  
  componentDidMount(){
   global.waterMark = 0;
   global.messageIndex = 0;
  //  this.getUser()
   this.timer = setInterval(()=> this.receiveMessage(), 5000)
   
  }
  renderDate = (date) => {
   return(
     <Text style={styles.time}>
       {date}
     </Text>
   );
 }
  render() {
    return (
      <View style={styles.container}>
      <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d',borderBottomWidth:0}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title style={styles.whiteColor}>Contrack FM</Title>
        </Body>
        <Right>
        <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
        </Button>
        <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
        </Button>
        </Right>
      </Header>
        <NavigationEvents
          onWillFocus={payload =>this.getUser() }
          onDidFocus={payload => console.log('will focus',payload)}
          onWillBlur={payload => this.setState({data:[]})}
          onDidBlur={payload => this.setState({data:[]})}
        />
         {this.state.ShowSpinner &&
          <View style={styles.spaceAround1}>
            <Spinner  color='red' />
          </View>
        }
        <FlatList style={styles.list}
        
        ref={ (ref) => { this.flatList = ref; }}
        
          data={this.state.data}
          keyExtractor= {(item) =>
             item.id.toString()
          }
          
          renderItem={(message) => {
           
            const item = message.item;
            let inMessage = item.type === 'in';
            let itemStyle = inMessage ? styles.itemIn : styles.itemOut;
            let itemColor = inMessage ? styles.itemRed :styles.itemWhite;
            return (
              <View style={[itemColor, itemStyle]}>
               
                <View style={[styles.balloon]}>
                  <Text>{item.message}</Text>
                
                </View>
                {this.renderDate(item.date)}
              </View>
            )
            
          }}
         
          />
         

        
<View style={[styles.btFooter]}>
<View>
<Item style={[styles.itemOverrideStyle,styles.spaceAround,styles.paddingTop]}> 
{this.state.actions.length >0 ?
  this.state.actions[0].map((r,k)=>
    <View key={k} style={[styles.padding]}>
      <Button  onPress={()=>this.sendMessageChoice(r.value)} style={[
          styles.button,
          styles.activeBtn,styles.padding ]}  rounded >
        <Text style={[
          styles.padding,styles.font12]} uppercase={false}>{r.value}</Text>
      </Button>
    </View>
    ):null}
  </Item>
  
</View> 

        <View style={[styles.footer]}>
  
  
        <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Write a message..."
                underlineColorAndroid='transparent'
                onChangeText={(message) => this.setState({message})} value={this.state.message}/>
               
           
            
               
          </View>
            <TouchableOpacity style={styles.btnSend}  onPress={(event) => this.sendMessage(event)}>
              <Image source={require('../../images/filled-sent.png')} style={styles.iconSend}  />
            </TouchableOpacity>
        </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  font12:{
    fontSize:12,
  },
  padding:{
    padding:5,
  },
  spaceAround1:{
    justifyContent:'space-around',
},
  spaceAround:{
    justifyContent:'flex-start',
},
  itemOverrideStyle:{
    borderBottomColor:'transparent',
     flexDirection: 'row', alignSelf: 'flex-start', flexWrap: 'wrap', width: '100%' 
  },
  viewWrap:{
    flexWrap:'wrap'
  },
  activeBtn:{
    backgroundColor: '#fff',
    color:'#E2E2E2'
},
  container:{
    flex:1,
    backgroundColor:'#E2E2E2',
  },
  list:{
    paddingHorizontal: 10,
  },
  btFooter:{
    flexDirection: 'column',
  },
  footer:{
    flexDirection: 'row',
    height:60,
    backgroundColor: '#eeeeee',
    paddingHorizontal:10,
    padding:5,
  },
  btnSend:{
    backgroundColor:"#e5322d",
    width:40,
    height:40,
    borderRadius:360,
    alignItems:'center',
    justifyContent:'center',
  },
  iconSend:{
    width:30,
    height:30,
    alignSelf:'center',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    height:40,
    flexDirection: 'row',
    alignItems:'center',
    flex:1,
    marginRight:10,
  },
  inputs:{
    height:40,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
  },
  balloon: {
    maxWidth: 250,
    padding: 15,
    borderRadius: 20,
  },
  itemIn: {
    alignSelf: 'flex-start'
  },
  itemOut: {
    alignSelf: 'flex-end'
  },
  time: {
    alignSelf: 'flex-end',
    margin: 15,
    fontSize:12,
    color:"#000",
  },
  itemWhite: {
    marginVertical: 14,
    flex: 1,
    flexDirection: 'row',
    backgroundColor:"#fff",
    borderRadius:20,
    padding:5,
  },
  itemRed: {
    marginVertical: 14,
    flex: 1,
    flexDirection: 'row',
    backgroundColor:"#e5322d",
    borderRadius:20,
    padding:3,
  },
  whiteColor:{
    color:'#fff'
  }
});
export default ChatScreen;
