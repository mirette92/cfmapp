import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather'
import {View} from 'react-native';
import {Container,Header,Content,Form,Item,Input,Button,Text,  Picker, Footer, Root ,Spinner, Toast} from 'native-base';
import styles from '../css/style'
import MyStatusBarCustom from '../../helpers/MyStatusBar';
import { Avatar } from 'react-native-elements';
import * as axios from 'axios';
import {AsyncStorage} from 'react-native';
import { Image } from 'react-native-elements';
import { RNCamera }  from 'react-native-camera';

class AddOrderScreen extends React.Component {
  constructor(props) {
    super(props);
    console.log(global.problems)
    this.state = {camera:false,img:'', description: '' ,external:'',branch:'',floor:'',room:'',building:'',when:'',problem:'',arrExternal:[],arrBranch:[],arrFloor:[],arrBuilding:[],arrProblem:[],company_name:'',spinner:false,disable:false};
   
  }
 
  listProblem(){
    console.log('hii');
    axios.get('http://contrackfm.bit68.com/api/problem/').then(response=> {
     
      
      this.setState({arrProblem:response.data})    
      
    })
    console.log(global.token)
    console.log('hii');
  }
  getUser= async()=>{
    let data = {
      method: 'GET',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    return fetch('http://contrackfm.bit68.com/api/profile/',data)
            .then(response => response.json())  // promise
            .then(json =>{
              
              if(json.status == 200){
                
                this.setState({'company_name':json.data[0].account[0].name})
                
                global.account = json.data[0].account;
                this.listBranch();
                
                
              }else{
                
              }

            } )
    
  }

  listBranch=async()=>{
    let data = {
      method: 'GET',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    axios.get('http://contrackfm.bit68.com/api/external_project/',data).then(response=> {
    
      const arrBranch = response.data.data;
      
      console.log(arrBranch);
      this.setState({arrBranch:arrBranch})
      
      
    })
  }
  listBuilding(index){
    
    const arrBuilding = this.state.arrBranch[index].buildings;
    console.log(arrBuilding)
    this.setState({arrBuilding:arrBuilding})
  }  
  listFloor=async()=>{
    
    let data = {
      method: 'GET',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    axios.get('http://contrackfm.bit68.com/api/building/'+this.state.building+'/',data).then(response=> {
    
      const arrFloor = response.data.floors;
      
      console.log(arrFloor);
      this.setState({arrFloor:arrFloor})
  })

  
}
  onBranchPickerValueChange=(value, index)=>{
    this.state.arrBranch.map((value1,index) => {
      if(value1.id == value){
        global.index_branch = index
      }
  });
    this.setState(
      {
        "branch": value
      },
      () => {
        // here is our callback that will be fired after state change.
        global.branch_id = value
        if(global.branch_id !== "0"){
          this.listBuilding(global.index_branch)
        }else{
          this.setState({arrFloor:[]})
        }
        
      }
    );
  }
  onBuildingPickerValueChange=(value, index)=>{
    this.setState(
      {
        "building": value
      },
      () => {
        // here is our callback that will be fired after state change.
        global.build_id = value
        if(global.build_id !== "0"){
          this.listFloor()
        }else{
          this.setState({arrFloor:[]})
        }
        
      }
    );
  }
  onProblemPickerValueChange=(value, index)=>{
    this.setState(
      {
        "problem": value
      },
     
    );
  }
 
  componentDidMount(){
    this.getUser();
    this.listProblem();
    
  
    
   }
  

  toasterfunc(message,type){
    Toast.show({
      text: message,
      buttonText: "Got it",
      position:'bottom',
      type:type
    })
  }
  
   submitOrder=async()=>{
     let strError='Enter '
     let x=0
    if(this.state.description == ''){
      strError +='description, '
      x = 1
    }if(this.state.branch ==''|| this.state.branch==0){
      strError +='branch, '
      x = 1
    }if(this.state.building ==''|| this.state.building==0){
      strError += 'building, '
      x = 1
    }if(this.state.floor ==''|| this.state.floor==0){
      strError +='floor, '
      x = 1
    }if(this.state.room ==''|| this.state.room==0){
      strError +='room, ';
      x = 1
    }if(this.state.problem ==''|| this.state.problem==0){
      strError +='problem ';
      x = 1
    }if(x==1){
      strError += 'to submit ';
      this.toasterfunc(strError,'danger')
      return 
    }
    
    this.setState({spinner:true,disable:true})

    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify({
        description: this.state.description ,
        external_project:this.state.branch,
        building:this.state.building,
        floor:this.state.floor,
        room:this.state.room,
        when:this.state.when,
        problem:this.state.problem,
        service:global.service,



      }),
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    return fetch('http://contrackfm.bit68.com/api/work_order/', data)
            .then(response => response.json())  // promise
            .then(json =>{
              
              if(json.status == 200){
                this.setState({spinner:false,disable:false,showToast:true,message:'account created successfully'})
                this.toasterfunc('Work order created successfully!','success')
               
                
              }else{
                this.toasterfunc('Failed to create Work order!','danger')
                this.setState({spinner:false,disable:false,showToast:true,message:'failed to create account'})
                
              }
      
            } )
            
    }

    takePicture = async function() {
      this.setState({spinner:true})
      if (this.camera) {
        const options = { quality: 0.5, base64: true };
        const data = await this.camera.takePictureAsync(options)
        console.log(data)
        this.setState({camera:false})
        this.setState({img:data.uri})
        this.setState({spinner:false}) 
        
      }
    };
    openCamera = function(){
      this.setState({camera:true})
    }
  render() {
    return (
      <Root>
      <Container>
      
        <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" style={{display:'none'}}/>
        {!this.state.camera ?
        <View style={styles.background}>
          <View style={[styles.iconRing,styles.paddingTop]}>
            <View style={[styles.image,styles.iconRing]}>
              <Avatar
              imageProps={{resizeMode:'center'}}
              size="large"
                rounded
                source={require('../../images/logocfm.png')}
              />
              <View style={{flexWrap:'wrap' ,paddingTop:20,paddingLeft:10}}>
              <Text  style={[styles.compNameStyle,styles.whiteColor,styles.padding_lt]}>{this.state.company_name}</Text>
              
            </View>
            
            </View>

          </View>
          <Item style={[styles.itemOverrideStyle,styles.spaceAround,styles.paddingTop]}> 
              <Text>{"\n"}</Text>
            </Item>
        </View>
        :null}
        <Content>
        {!this.state.camera ?
          <Form >
            <Item Input style={[styles.QuestionaireForm,styles.paddingTop]}>
              <View style={styles.flowDirectionColView}>
                <Text style={styles.quesTextStyle}>Work order description?</Text>
              
                <Input placeholder="please write the problem to fix it" onChangeText={(description) => this.setState({description})} placeholderTextColor="#E2E2E2" style={[styles.fontSize,styles.grayColor]}/>
              </View>
            </Item>
           
            
            <Text style={styles.quesTextStyle}>Where?</Text>
            <Item picker style={styles.bottomBorder0}>
              <Picker
               selectedValue={this.state.branch}
                onValueChange={this.onBranchPickerValueChange}
                mode="dropdown"
                iosIcon={<Ionicons name="ios-arrow-down" />}
                style={styles.dropDown}
                placeholder="Branch / Department"
                placeholderStyle={styles.lightGray}
                placeholderIconColor="#007aff"
                
              >
              <Picker.Item label='Branch / Department' value='0' />
             { this.state.arrBranch != undefined ?
                  this.state.arrBranch.map( (v,index)=>{
                  return <Picker.Item label={v.name+'-'+v.Account.name} key={index} value={v.id} />
                  }):null
                }
              </Picker>
            </Item>
            <Item style={[styles.QuestionaireForm]}>
              
            </Item>
            <Item picker style={styles.bottomBorder0}>
              <Picker
               selectedValue={this.state.building}
               onValueChange={this.onBuildingPickerValueChange }
              
                mode="dropdown"
                iosIcon={<Ionicons name="ios-arrow-down" />}
                style={styles.dropDown}
                placeholder="building"
                placeholderStyle={styles.lightGray}
                placeholderIconColor="#007aff"
                
              >
              <Picker.Item label='Building' value='0' />
             { this.state.arrBuilding != undefined ?
                  this.state.arrBuilding.map( (v,index)=>{
                  return <Picker.Item label={v.name} key={index} value={v.id} />
                  }):null
                }
              </Picker>
            </Item>
            <Item style={[styles.QuestionaireForm]}>
              
            </Item>
            <Item picker style={styles.bottomBorder0}>
              <Picker
                selectedValue={this.state.floor}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({floor: itemValue})
                }
                mode="dropdown"
                iosIcon={<Ionicons name="ios-arrow-down" />}
                style={styles.dropDown}
                placeholder="Floor"
                placeholderStyle={styles.lightGray}
                placeholderIconColor="#007aff"
                
              >
                <Picker.Item label="Floor" value='0' />
                { this.state.arrFloor != undefined ?
                  this.state.arrFloor.map( (v,index)=>{
                  return <Picker.Item label={v.name} key={index} value={v.id} />
                  }):null
                }
            </Picker>
            </Item>
            <Item style={[styles.QuestionaireForm]}>
              
            </Item>
            
            <Input placeholder="Room" onChangeText={(room) => this.setState({room})} placeholderTextColor="#E2E2E2" style={[styles.fontSize,styles.grayColor,styles.padding_lt]}/>
           
            <Item style={[styles.QuestionaireForm]}>
              
            </Item>
            <Text style={styles.quesTextStyle}>When?</Text>
            <Item picker style={styles.bottomBorder0}>
              <Picker
                selectedValue={this.state.when}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({when: itemValue})
                }
                mode="dropdown"
                iosIcon={<Ionicons name="ios-arrow-down" />}
                style={[styles.dropDown,styles.grayColor,styles.fontSize]}
                placeholder="Time"
                placeholderStyle={styles.lightGray}
                placeholderIconColor="#007aff"
              >
                <Picker.Item label='Time' value='0' />
                <Picker.Item label="During Work hr" value="1" />
                <Picker.Item label="After Work hr" value="2" />
              </Picker>
            </Item>
            <Item style={[styles.QuestionaireForm]}>
              
            </Item>
           
            <Text style={styles.quesTextStyle}>Problem Type?</Text>
            <Item picker style={styles.bottomBorder0}>
              <Picker
                mode="dropdown"
                selectedValue={this.state.problem}
                onValueChange={this.onProblemPickerValueChange}
                iosIcon={<Ionicons name="ios-arrow-down" />}
                style={styles.dropDown}
                placeholder="Problem Type"
                placeholderStyle={styles.lightGray}
                placeholderIconColor="#007aff"
              >
                <Picker.Item label="Problem Type" value='0' /> 
                { this.state.arrProblem != undefined ?
            
                  this.state.arrProblem.map( (v,index)=>{
                
                  return <Picker.Item label={v.name} key={index} value={v.id} />
                  }):null
                }
                 
              </Picker>
            </Item>
            <Item style={[styles.QuestionaireForm]}>
              
            </Item>
           
            <Item style={[styles.itemOverrideStyle,styles.spaceAround,styles.paddingTop]}> 
           
              <Button  style={[styles.button,styles.activeBtn]} rounded onPress={(event) => this.openCamera(event)}>
                  <Text style={ styles.whiteColor } uppercase={false}>Upload Photo</Text>
              </Button>
            
              
              <Button  style={[styles.button,styles.activeBtn]} rounded >
                <Text style={ styles.whiteColor } uppercase={false}>Upload Barcode</Text>
              </Button>
              
            </Item>
            {this.state.img != ''?
              
            <Item style={[styles.itemOverrideStyle,styles.spaceAround,styles.paddingTop]}>
             
              <Image
                  source={{ uri: this.state.img }}
                  style={{ width: 200, height: 200 }}

                />
            </Item>
            :null}
            <Item style={[styles.itemOverrideStyle,styles.paddingTop,styles.contentCenter]}> 
              <Button block  rounded style={styles.buttonSubmitDefaultStyle} onPress={(event) => this.submitOrder(event)}>
              {this.state.spinner ? <Spinner  color='#E2E2E2' />:<Text uppercase={false}>Submit</Text>}
              </Button>
            </Item>
            <Item style={[styles.itemOverrideStyle,styles.spaceAround,styles.paddingTop]}> 
              <Text>{"\n"}</Text>
            </Item>
          </Form>
          :
          <View style={styles.containerC}>
          
            <RNCamera
              ref={cam => {this.camera = cam}}
              style={styles.preview}
              type={RNCamera.Constants.Type.back}
            >
            {this.state.spinner &&
            <View style={styles.spaceAround}>
              <Spinner  color='red' />
            </View>
          } 
            <Text style={styles.capture} onPress={this.takePicture.bind(this)}>
              [CAPTURE]
            </Text>
            </RNCamera>
          </View>
          }
        </Content>
      </Container>
      </Root>       
       
    );
  }
}

export default AddOrderScreen;
