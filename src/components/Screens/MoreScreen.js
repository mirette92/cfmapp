import React, {Component} from 'react';


import { Container, Header, Content, List, ListItem, Text, Icon, Right,Body,Title,Left,Button} from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet} from 'react-native';
import MyStatusBarCustom from '../../helpers/MyStatusBar';
import styles from '../css/style'
import {AsyncStorage} from 'react-native';
class MoreScreen extends React.Component {
  
  signout= async()=>{
    
    await AsyncStorage.removeItem('token');
   
    var value=await AsyncStorage.getItem('token');
  
    if(value == null){
      
      this.props.navigation.navigate('Signin');
    }
    console.log('helloo')
  }
  render() {
    return (

      <Container >
        <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d',borderBottomWidth:0}}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack(null)}>
              <Ionicons style={styles.whiteColor} size={20} name="ios-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.whiteColor}>More</Title>
          </Body>
          <Right>
            <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
              <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
            </Button>
            <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
              <Ionicons size={30} style={styles.whiteColor} name="ios-chatboxes" />
            </Button>
          </Right>
        </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>
          <List>
            <ListItem noIndent onPress={() => this.props.navigation.navigate('Offers')} style={styles.whiteBackgroundColor} >
              <Feather name="book-open" style={styles.redColor}  size={18} />
              <Text style={styles.TextStyle_lt}>Offers</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>
              <Ionicons name="ios-mail" style={styles.redColor} size={18} />
              <Text style={styles.TextStyle_lt}>Contact Us</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>
              <FontAwesome name="lock" style={styles.redColor} size={18} />
              <Text style={styles.TextStyle_lt}>Privacy Policy</Text>

            </ListItem>
            <ListItem noIndent onPress={() => this.props.navigation.navigate('About')} style={styles.whiteBackgroundColor}>
              <Ionicons name="md-contact" style={styles.redColor} size={18} />
              <Text style={styles.TextStyle_lt}>About Us</Text>

            </ListItem>
            <ListItem noIndent onPress={() => this.props.navigation.navigate('Faq')} style={styles.whiteBackgroundColor}  >
              <FontAwesome name="list" style={styles.redColor} size={18} />
              <Text style={styles.TextStyle_lt}>FAQs & terms</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor} onPress={() => this.props.navigation.navigate('Profile')}>
              <FontAwesome name="user" style={styles.redColor} size={18} />
              <Text style={styles.TextStyle_lt}>Profile</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor} onPress={() => this.signout()}>
              <FontAwesome name="power-off" style={styles.redColor} size={18} />
              <Text style={styles.TextStyle_lt}>Signout</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>

              <Text style={styles.TextStyleVersion}>V 1.0.1</Text>

            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

export default MoreScreen;
