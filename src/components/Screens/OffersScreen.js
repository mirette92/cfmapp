//imports
import React ,{Component} from 'react';

import {View, Image ,ImageBackground} from 'react-native';

import { Container, Header, Content, Text, Right,Left,Body,Title,Button,Card,Spinner} from 'native-base';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MyStatusBarCustom from '../../helpers/MyStatusBar';
import styles from '../css/style';
import * as axios from 'axios';
//body(class-[functions-methodes-jsx])
class OffersScreen extends Component{
  state = {
    arrOffer: [],
    ShowSpinner:true
  }
  
  listOffer(){
    console.log('hii');
    axios.get('http://contrackfm.bit68.com/api/offer/').then(response=> {
     
      const arrOffer = response.data;
      this.setState({ShowSpinner:false});
      this.setState({arrOffer:arrOffer})
      console.log(arrOffer);
      
    })
    
  }
  componentDidMount(){
   this.listOffer();
  }

  render(){
    return(

    <Container >
      <MyStatusBarCustom backgroundColor = "#952421"/> 
      <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d',borderBottomWidth:0,}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <Ionicons size={20} style={{color:'#fff'}} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title style={styles.whiteColor}>Offers</Title>
        </Body>
        <Right>
        <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
        </Button>
        <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
          <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
        </Button>
        </Right>
      </Header>
      <ImageBackground style={[styles.backgroungImgView,styles.contentCenter]} imageStyle={styles.image}  source={require('../../images/home-background.jpg')}>
        <Image style={styles.logo} source={require('../../images/CFM-logo-white.png')}/>
      </ImageBackground>
      <Content>
      {this.state.ShowSpinner &&
          <View style={styles.spaceAround}>
            <Spinner  color='red' />
          </View>
        }
       {this.state.arrOffer.map((Offer,key)=> 
       <Card transparent style={[styles.offerCard,styles.paddingTop]} key={key}>  
              
          <View >
            <Image source={{uri:Offer.img}} style={styles.ImgOffer}  />
          </View>
          <Text style={styles.padding_lt}>
          <Text >{Offer.description} </Text> {"\n"} <Text style={[styles.redColor,styles.underline]}>Get Offer</Text>
          </Text>
        </Card>
        )}
      </Content>
    </Container>
    )
  }
}


//css (styling)



//exports
export default OffersScreen;
