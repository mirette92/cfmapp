import React, {Component} from 'react';
import { View} from 'react-native';
import { Container, Header, Content, Card, CardItem, Body,Left,Right,Title,Button,Textarea,Item,Accordion } from "native-base";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Rating, AirbnbRating } from 'react-native-ratings';
import StarRating from 'react-native-star-rating';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar,Text } from 'react-native-elements';
import styles from '../css/style';

import MyStatusBarCustom from '../../helpers/MyStatusBar';



const STAR_IMAGE = require('../../images/star.png')

class ConfirmWorkOrderScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    rate:'Good',
    collapsed:false,//do not show the body by default
    starCount: 3
    }
  }
  onPressLogin = (event)=>{
    alert('you logged successfully');
  }
 
  ratingCompleted(rating) {
    if(rating == 5){
      this.setState({rate:'Excellent',starCount: rating}) ;
    }else if(rating == 4){
      this.setState({rate:'Very Good',starCount: rating}) ;
    }else if(rating == 3){
      this.setState({rate:'Good',starCount: rating}) ;
    }else if(rating == 2){
      this.setState({rate:'Bad',starCount: rating}) ;
    }else if(rating == 1){
      this.setState({rate:'Terrible',starCount: rating}) ;
    }else{
      this.setState({rate:'Terrible',starCount: rating}) ;
    }
  }
    
  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }
 
  render() {
    return (
      <Container>
        <MyStatusBarCustom backgroundColor = "#952421"/> 
        
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d'}}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack(null)}>
              <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.whiteColor}>Confirm WO</Title>
          </Body>
          <Right>
            <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
              <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
            </Button>
            <Button transparent onPress={() => this.props.navigation.navigate('Chat')}>
              <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
            </Button>
          </Right>
        </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>

          <Card style={styles.whitebackground}>
          <CardItem>
            <Body >
              <View style={styles.offerCard}>
              <Avatar
              size="large"
              rounded
              source={{
                uri:
                  'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}
                />
                <View style={[styles.flowDirectionColView,styles.paddingTop]}>
                  <Text style={[styles.fontSize,styles.lightGrayColor,styles.padding_lt]}>
                    Vodafone
                  </Text>
                  <Text style={[styles.fontSize,styles.lightGrayColor,styles.padding_lt]}>
                    250 Work Order
                  </Text>
                  <Text style={[styles.fontSize12,styles.lightGrayColor,styles.padding_lt]}>
                  Job Task
                  </Text>
                  <Text style={[styles.fontSize12,styles.lightGrayColor,styles.padding_lt]}>
                    Tap ,wash basee in and sink problem
                  </Text>
                  <Text style={[styles.fontSize12,styles.lightGrayColor,styles.padding_lt]}>
                    Address
                  </Text>
                  <Text style={[styles.fontSize12,styles.lightGrayColor,styles.padding_lt]}>
                    NileCity building , cairo ,egypt
                  </Text>
                </View>
              </View>
            </Body>
          </CardItem>
        </Card>
        <Card >
        <Item style={[styles.itemOverrideStyle,styles.paddingTop,styles.contentCenter]}> 
          <Text style={[styles.redColor,styles.fontSize18,styles.bold]}>{this.state.rate}</Text>
        </Item>
        <Item style={[styles.itemOverrideStyle,styles.paddingTop,styles.contentCenter]}> 
          
      
            
            <StarRating
            style={{ width: 60 }}
              disabled={false}
              maxStars={5}
              rating={this.state.starCount}
              selectedStar={(rating) => this.ratingCompleted(rating)}
              fullStarColor={'#e5322d'}
              
            />
        
        </Item>
        <Item style={[styles.itemOverrideStyle,styles.paddingTop,styles.contentCenter]}> 
          <Button block  rounded style={[styles.buttonSubmitWhiteDefaultStyle,styles.contentCenter,styles.paddingBottom]} onPress={()=>this.setState({collapsed:!this.state.collapsed})}>
            <Text uppercase={false} style={[styles.redColor]}>Add Comment</Text>
          </Button>
        </Item>
          <Collapse isCollapsed={this.state.collapsed} 
	        onToggle={(isCollapsed)=>this.setState({collapsed:isCollapsed})}>
          
            <CollapseHeader >
            </CollapseHeader>
        
            <CollapseBody style={[styles.itemOverrideStyle,styles.paddingTop,,styles.contentCenter]}>
              <View style={[styles.paddingMarginTextarea,styles.centerContent,styles.padding_lt,styles.paddingTop]}>
                <Textarea rowSpan={5}  placeholder="Textarea" />
              </View>
            </CollapseBody>
          </Collapse>
          <View style={[styles.paddingTop,,styles.contentCenter]}>        
           
            <View style={[styles.paddingMarginTextarea,styles.centerContent,styles.padding_lt,styles.paddingTop]}>
                <Textarea rowSpan={5}  placeholder="Signture Authorization" />
            </View>
          </View>
          <Item style={[styles.itemOverrideStyle,styles.spaceAround,styles.paddingTop]}> 
          <Text>{"\n"}</Text>
        </Item>
        </Card>
       
        <Item style={[styles.itemOverrideStyle,styles.contentCenter]}> 
          <Button block  rounded style={[styles.buttonSubmitDefaultStyle,styles.paddingBottom]} onPress={() => this.props.navigation.navigate('Order')}>
            <Text uppercase={false} style={styles.whiteColor}>Approved</Text>
          </Button>
        </Item>
        <Item style={[styles.itemOverrideStyle,styles.spaceAround,styles.paddingTop]}> 
          <Text>{"\n"}</Text>
        </Item>
        </Content>

      </Container>

    );
  }

}


export default ConfirmWorkOrderScreen;
