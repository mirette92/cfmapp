import React, {Component} from 'react';


import { Container, Header, Content, List, ListItem, Text, Icon, Right ,Left,Button,Body,Title} from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet} from 'react-native';
import MyStatusBarCustom from '../../helpers/MyStatusBar';

import styles from '../css/style';

class NotificationScreen extends React.Component {



  render() {
    return (
      <Container >
        <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d',borderBottomWidth:0}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={styles.whiteColor} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
          <Body>
            <Title style={styles.whiteColor}>Notification</Title>
          </Body>
          <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
          </Button>
          <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={styles.whiteColor} name="ios-chatboxes" />
          </Button>
          </Right>
        </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>
          <List>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>

              <Text style={styles.TextStyle}>Your techncian has arrived.{"\n"}work order #122345</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>

              <Text style={styles.TextStyle}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>

              <Text style={styles.padding_lt}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>

              <Text style={styles.padding_lt}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>

              <Text style={styles.padding_lt}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>
            <ListItem noIndent  style={styles.whiteBackgroundColor}>

              <Text style={styles.padding_lt}>Work Order #122345 has been authorized {"\n"} to be assigned to a techncian</Text>

            </ListItem>

          </List>
        </Content>
      </Container>
    );
  }
}

export default NotificationScreen;
