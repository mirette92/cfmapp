import React, {Component} from 'react';


import { Container, Header, Content, List,  Text, Icon, Right,Left,Button,Title,Body,Accordion,Spinner,Toast,Root} from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Feather from 'react-native-vector-icons/Feather'
import {StyleSheet,View} from 'react-native';
import { ListItem } from 'react-native-elements'
import { withNavigation } from 'react-navigation';
import MyStatusBarCustom from '../../helpers/MyStatusBar';
import styles from '../css/style';
import * as axios from 'axios';
import {AsyncStorage} from 'react-native';
import { NavigationEvents } from 'react-navigation';


class EmployeesScreen extends React.Component {
  constructor(props) {
    super(props);
    this._renderHeader = this._renderHeader.bind(this);
    this._renderContent = this._renderContent.bind(this);
}

state = {
  arrWorkOrder: [],
  ShowSpinner:true,
  spinner:false,
  user:''
}

  acceptWO(item){
    
    this.setState({spinner:true})
    this.toasterfunc('request sent wait for code!','success')
    item.status = 1
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify(item),
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        
      }
    }
    
    return fetch('http://contrackfm.bit68.com/api/send-to-ms/', data)
    .then(response => response.json())  // promise
    .then(json =>{
      if(json.status == 200){
        this.setState({spinner:false})
        alert('success Wocode:'+json.wo_code)
        this.listWorkOrder();
      }else{
        alert('failed try again')
        this.setState({spinner:false})
      }
      console.log('hello')
        // if(json.status == 1){
          
        //   this.toasterfunc('work oredr accept!','success')
        // }else{
        //   this.toasterfunc('work order reject!','danger')
        // }
    }).catch((error) => {
      console.log(error);
}
) 
  }
  editWo(item){
    global.wo = item
    this.props.navigation.navigate('AddOrder');
  }
  rejectWO(item){
    item.status = 0
   

    let data = {
      method: 'PUT',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify(item),
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        
      }
    }
    return fetch('http://contrackfm.bit68.com/api/work_order/'+item.id+'/', data)
    .then(response => response.json())  // promise
    .then(json =>{
        if(json.status == 0){
          
          var index = this.state.arrWorkOrder.indexOf(item);
          this.state.arrWorkOrder.splice(index, 1);
          var arrWorkOrder = this.state.arrWorkOrder;
          this.setState({arrWorkOrder:arrWorkOrder})
          
          this.toasterfunc('work oredr accept!','success')
        }else{
          this.toasterfunc('work order reject!','danger')
        }
    })
  }
  getUser= async()=>{
    let data = {
      method: 'GET',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    return fetch('http://contrackfm.bit68.com/api/profile/',data)
            .then(response => response.json())  // promise
            .then(json =>{
              
              if(json.status == 200){
                
                this.setState({'company_name':json.data[0].account[0].acount_name})
                this.setState({'user':json.data[0]})
                global.account = json.data[0].account;
                
                
              }else{
                
              }

            } )
    
  }
  toasterfunc(message,type){
    Toast.show({
      text: message,
      buttonText: "Got it",
      position:'bottom',
      type:type
    })
  }
  listWorkOrder=async()=>{
    let data = {
      
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    axios.get('http://contrackfm.bit68.com/api/work_order?status=-1&status1=-1&status2=-1&status3=-1',data).then(response=> {

      const arrWorkOrder = response.data.arrWorkOrder;
      this.setState({ShowSpinner:false});
      console.log(arrWorkOrder);
      this.setState({arrWorkOrder:arrWorkOrder})
      
      
    }).catch(function(error){
      console.log('there is'+ error.message)
    })
  } 
  
  componentDidMount(){
    this.listWorkOrder();
    this.getUser()
    
   } 
  _renderHeader(item, expanded) {
    return (
      <View style={[styles.empolyeeView,styles.whiteBackgroundColor]}>
          <View style={styles.employeeViewName}>
            <FontAwesome name="user" style={styles.redColor} size={40} />
            <Text style={styles.padding_lt}>{" "}{item.user.name} - {item.external_project.name}.</Text>
            
          </View>
        {expanded
          ? <Icon style={styles.fontSize18} name="remove" />
          : <Icon style={styles.fontSize18} name="add" />}
      </View>
    );
  }
  _renderContent(item) {
    return (
      <View >
       {this.state.user.is_admin_app  && item.status == -1 ?
        <View style={styles.ImgTextInput}>
        
          <Button transparent onPress={() => this.acceptWO(item)}>
         
            <MaterialIcons name="done" color="green" size={20} />
            <Text style={{color:'green'}}>Accept</Text>
      
          </Button>
        
          {/* <Button transparent  onPress={() => this.editWo(item)}>
            <FontAwesome5 name="edit" color="green" size={20} />
            <Text style={{color:'green'}}>Edit</Text>
          </Button> */}
          <Button transparent  onPress={() => this.rejectWO(item)}>
            <Ionicons name="ios-close" color="#e5322d" size={20} />
            <Text style={{color:'#e5322d'}}>reject</Text>
          </Button>


      </View>:null
       }
       {this.state.spinner &&
          <View style={styles.spaceAround}>
            <Spinner  color='red' />
          </View>
        } 
       <Text
        style={styles.textStyleEmployee}
      >
        Status :{item.status == -1 && <Text style={{color:"gray"}}>"Pending"</Text> }{item.status == 0 && <Text style={{color:"red"}}>"Rejected"</Text> }{item.status == 1 && <Text style={{color:"green"}}>"Open - Unscheduled"</Text> }
      </Text>
       <Text
        style={styles.textStyleEmployee}
      >
        account :{item.account.name}
      </Text>
      <Text
        style={styles.textStyleEmployee}
      >
        External Project :{item.external_project.name}
      </Text>
      <Text
        style={styles.textStyleEmployee}
      >
        building :{item.building.name}
      </Text>
      <Text
        style={styles.textStyleEmployee}
      >
        floor :{item.floor.name}
      </Text>
      <Text
        style={styles.textStyleEmployee}
      >
        Service :{item.service.name}
      </Text>
      <Text
        style={styles.textStyleEmployee}
      >
          Problem :{item.problem.name}
      </Text>
      
      <Text
       style={styles.textStyleEmployee}
      >
        Description :{item.description}
      </Text>
      
      </View>
    );
  }
  
  render() {
    return (
      <Root>
      <Container >
      <NavigationEvents
      onWillFocus={payload => console.log('will focus',payload)}
      onDidFocus={payload => this.listWorkOrder()}
      onWillBlur={payload => this.setState({arrWorkOrder:[],ShowSpinner:true})}
      onDidBlur={payload => this.setState({arrWorkOrder:[],ShowSpinner:true})}
    />
        <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d',borderBottomWidth:0}}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack(null)}>
              <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.whiteColor}> Organization</Title>
          </Body>
          <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
          </Button>
          <Button transparent  onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={styles.whiteColor} name="ios-chatboxes" />
          </Button>
          </Right>
        </Header>
        
        <Content style={{backgroundColor:'#E2E2E2'}}>
        {this.state.ShowSpinner &&
          <View style={styles.spaceAround}>
            <Spinner  color='red' />
          </View>
        } 
        <Accordion dataArray={this.state.arrWorkOrder} renderHeader={this._renderHeader}
            renderContent={this._renderContent} expanded={0}/>

        </Content>
      </Container>
      </Root>
    );
  }
}


export default  withNavigation(EmployeesScreen);
