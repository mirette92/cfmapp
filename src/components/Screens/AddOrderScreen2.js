import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {Button} from 'native-base';
import { RNCamera }  from 'react-native-camera';
import AddOrderScreen from './AddOrderScreen';
import styles from '../css/style'
import {AsyncStorage} from 'react-native';


class AddOrderScreen2 extends Component {
  render() {
    return (
      <View style={styles.containerC}>
       <RNCamera
        ref={cam => {this.camera = cam}}
        style={styles.preview}
        type={RNCamera.Constants.Type.back}
      >
        <Text style={styles.capture} onPress={this.takePicture.bind(this)}>
          [CAPTURE]
        </Text>
      </RNCamera>
      </View>
    );
  }

  takePicture = async function() {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options)
      global.photoTake = data.uri
      console.log(data);
      this.props.navigation.navigate('AddOrder');
    }
  };
}


//AppRegistry.registerComponent('AddOrderScreen2', () => BadInstagramCloneApp);
export default AddOrderScreen2