
import React ,{Component} from 'react';
import {View,StyleSheet,Image} from 'react-native';
import { Container, Header, Content, List, ListItem, Text,Spinner, Icon, Right ,Left,Button,Body,Title} from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import styles from '../css/style'
import {AsyncStorage} from 'react-native';
import * as axios from 'axios';
class Completed extends Component{
  state = {
    arrWorkOrder: [],
    ShowSpinner:true,
    user:''
  }
  listWorkOrder=async()=>{
    let data = {
      
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    axios.get('http://contrackfm.bit68.com/api/work_order?status=5&status1=6&status2=5&status3=5',data).then(response=> {

      const arrWorkOrder = response.data.arrWorkOrder;
      this.setState({ShowSpinner:false});
      console.log(arrWorkOrder);
      this.setState({arrWorkOrder:arrWorkOrder})
      
      
    }).catch(function(error){
      console.log('there is'+ error.message)
    })
  } 
  componentDidMount(){
    this.listWorkOrder();
    
   } 
    render()
    {
      return(
        <Container>
        <Content style={{backgroundColor:'#E2E2E2'}}>
          {this.state.ShowSpinner &&
            <View style={styles.spaceAround}>
              <Spinner  color='red' />
            </View>
          } 
          {this.state.arrWorkOrder.length == 0 && this.state.ShowSpinner == false ?
        <View style={{ justifyContent:'space-around', }}>
          <Image
          source={require('../../images/no.png')}
          style={{ width: 200, height: 200, justifyContent:'center'}}

          />
        </View>
    
        :
          <List>

          {this.state.arrWorkOrder.map((prop, key) => {
             return(
            <ListItem noIndent key={key} style={{backgroundColor:'#fff'}}>
              <MaterialIcons name="done" color="#e5322d" size={20} />
              <Text style={styles.padding_lt}>
              <Text>{prop.user.name}</Text>{"\n"}
                <Text style={styles.padding_lt}>Service :{prop.service.name}.{"\n"}#{prop.cfm_wo_code}</Text>{"\n"}
                <Text>{prop.status== 5 ? 'Closed-Completed': 'Closed-Posted' }</Text>{"\n"}
                {prop.status == 5 ?<Text style={[styles.redColor,styles.underline]}>Rate Service</Text> :null}
                
              </Text>
            </ListItem>
            )
             })}
          
          </List>
          }
        </Content>
        </Container>
      )
    }

}

export default Completed;
