import React, {Component} from 'react';
import {Platform, StyleSheet,  View,Image} from 'react-native';
import { Container, Header, Content, Card, CardItem, Text, Body,Left,Right,Title,Button,Input } from "native-base";
import { createStackNavigator, createAppContainer } from "react-navigation";

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar } from 'react-native-elements';
import MyStatusBarCustom from '../../helpers/MyStatusBar';

import {AsyncStorage} from 'react-native';
import styles from '../css/style';




class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { edit:false,user:{account:[]}};
   
  }
 
  getUser= async()=>{
    let data = {
      method: 'GET',
      credentials: 'same-origin',
      mode: 'same-origin',
      
      headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+ await AsyncStorage.getItem('token')
        
      }
    }
    return fetch('http://contrackfm.bit68.com/api/profile/',data)
            .then(response => response.json())  // promise
            .then(json =>{
              if(json.status == 200){
                this.setState({'user':json.data[0]})
              }else{
                
              }

            } )
    
  }
  componentDidMount(){
    this.getUser();
   
   }
  render() {
    return (
      <Container>
        
      <MyStatusBarCustom backgroundColor = "#952421"/> 
        <Header androidStatusBarColor="#952421" noShadow style={{backgroundColor:'#e5322d',borderBottomWidth:0}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.goBack(null)}>
            <Ionicons style={{color:'#fff'}} size={20} name="ios-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title style={styles.whiteColor}>My Profile</Title>
        </Body>
        <Right>
          <Button transparent  onPress={() => this.props.navigation.navigate('Notification')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-notifications" />
          </Button>
          <Button transparent onPress={() => this.props.navigation.navigate('Chat')}>
            <Ionicons size={30} style={{color:'#fff'}} name="ios-chatboxes" />
          </Button>
        </Right>
      </Header>
        <Content style={{backgroundColor:'#E2E2E2'}}>

          <View style={styles.container}>
          <View style={styles.header}>
               
            <View style={styles.headerContent}>
                <Image style={styles.avatar}
                  source={require('../../images/no-profile-image.jpg')}/>

                <Text style={styles.name}>
                {this.state.user.name}
                </Text>
                
            </View>
          </View>

          <View style={styles.body}>
            <View style={styles.bodyContent}>
                <Text style={styles.textInfo}>
                  {this.state.user.email}
                </Text>
            
                <Text style={styles.textInfo}>
                  Mobile:  +20{this.state.user.mobile != '' ? this.state.user.mobile:'N/A'}
                </Text>
              
                <Text style={styles.textInfo}>
                  Company Name: {this.state.user.account.map((value,index)=>
                
                  <Text key={index}>
                    <Text style={styles.textInfo}>{value.name}</Text>
                  </Text>
                    
                  )} 
                </Text>
               
                
            </View>
          </View>
          
            
          </View>
        </Content>

      </Container>

    );
  }
}


export default ProfileScreen;
