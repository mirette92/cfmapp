//imports
import React ,{Component} from 'react';
import {Container,Header,Content,Form,Item,Input,Button,Text, Card, Toast,Root,Spinner} from 'native-base';
import { ImageBackground,View, Image } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {AsyncStorage} from 'react-native';

import styles from '../css/style';
import * as axios from 'axios';
import { withNavigation } from 'react-navigation';
//body(class-[functions-methodes-jsx])
class SigninScreen extends Component{
  constructor(props){
    super(props);
    this.state={
    username:'',
    password:'',
    spinner:false,
    disable:false
    }
    
    this.isLogin();
  }
  isLogin= async()=>{
    var value=await AsyncStorage.getItem('token');
  
  if(value !== null){
    this.props.navigation.navigate('Order');
  }
  }
  handleChange = (textValue)=>{
    this.setState({username: textValue})
  }
  toasterfunc(message,type){
    Toast.show({
      text: message,
      buttonText: "Got it",
      position:'up',
      type:type
    })
  }
  handleClick(event){
    this.setState({spinner:true,disable:true})
      console.log(this.state.username + this.state.password)

      //const user = {username:this.state.username, password:this.state.password}

      let data = {
        method: 'POST',
        credentials: 'same-origin',
        mode: 'same-origin',
        body: JSON.stringify({
          username:this.state.username,
           password:this.state.password
        }),
        headers: {
          'Accept':       'application/json',
          'Content-Type': 'application/json',
          
        }
      }
      return fetch('http://contrackfm.bit68.com/api/userlogin/', data)
              .then(response => response.json())  // promise
              .then(async(json) =>{
                
                if(json.status == undefined){
                  this.toasterfunc('Login successfully!','success')
                  this.setState({spinner:false,disable:false})
                  
                  await AsyncStorage.setItem('token', json.token);
                   
                  global.token = json.token
                  this.props.navigation.navigate('Order');
                }else{
                  this.toasterfunc('Login failed!','danger')
                  this.setState({spinner:false,disable:false})
                }

              } )
       
     
  }
  render(){
    return(
      <Root>
        <Container>
          <Header androidStatusBarColor="#952421" style={{display:'none'}}/>
          <ImageBackground style={[styles.backgroungImgView,styles.contentCenter]} imageStyle={styles.image}  source={require('../../images/home-background.jpg')}>
            <Image style={styles.logo} source={require('../../images/CFM-logo-white.png')}/>
          </ImageBackground>
          <View style={[styles.contentCenter]}>
            <Text style={[styles.grayColor,styles.fontSize12,styles.paddingTop]}>Sign in or Sign up to continue</Text>
          </View>
          <Content >
            <Form >
              <Item style={[styles.ItemForm,styles.paddingTop]}>
              
                <FontAwesome name="user" color="#e5322d" size={15} />
                <Input onChangeText={(username) => this.setState({username})} placeholder="Enter Username" style={[styles.fontSize,styles.blackColor,styles.padding_lt]} />
              </Item>
              
              <Item style={[styles.ItemForm,styles.contentCenter,styles.paddingTop]}>
          
                  <Ionicons name="ios-key" color="#e5322d" size={15} />
                  <Input placeholder="Enter Password" onChangeText={(password) => this.setState({password})} style={[styles.fontSize,styles.blackColor,styles.padding_lt]}  secureTextEntry={true}/>
              
              </Item>
              <Item style={[styles.itemOverrideStyle,styles.paddingTop,styles.contentCenter]}> 
                <Button disabled={this.state.disable} block  rounded style={styles.buttonSubmitDefaultStyle} onPress={(event) => this.handleClick(event)}>
                {this.state.spinner ? <Spinner  color='#E2E2E2' />:<Text uppercase={false}>Sign In</Text>}
                </Button>
              </Item>
            </Form>
            <Card transparent style={[styles.inline,,styles.paddingTop]}>
              <Button transparent onPress={() => this.props.navigation.navigate('Signup')}>
                <Text uppercase={false} style={styles.redColor}>Signup</Text>
                <Text style={styles.redColor}>|</Text>
              </Button>
              
              <Button transparent onPress={() => this.props.navigation.navigate('ForgetPassword')}>
                <Text uppercase={false} style={styles.redColor}>Forget Password</Text>
              </Button>
            </Card>
          </Content>
        </Container>
      </Root>
    )
  }
}





//exports
export default withNavigation(SigninScreen) ;
