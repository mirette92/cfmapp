import React ,{Component} from 'react';
import {StatusBar,Platform,StyleSheet,View} from 'react-native';
import {  Right,Left,Body,Title,Button, Content} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

const MyStatusBar = ({backgroundColor, ...props}) => (
 
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
       
 
  );

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const styles = StyleSheet.create({
    statusBar: {
      height: STATUSBAR_HEIGHT,
    },
   
})

export default MyStatusBar;