/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import {Root} from 'native-base'
//NavigationLibary
import { createStackNavigator,createBottomTabNavigator,createAppContainer ,NavigationActions ,withNavigation} from "react-navigation";
//Screens
import SignupScreen from './src/components/Screens/SignupScreen'
import SigninScreen from './src/components/Screens/SigninScreen'
import ForgetPasswordScreen from './src/components/Screens/ForgetPasswordScreen'
import NotificationScreen from './src/components/Screens/NotificationScreen'
import AboutScreen from './src/components/Screens/AboutScreen'
import FaqsScreen from './src/components/Screens/FaqsScreen'
import WorkScreen from './src/components/Screens/WorkScreen'
import OffersScreen from './src/components/Screens/OffersScreen'
import MoreScreen from './src/components/Screens/MoreScreen';
import ProfileScreen from './src/components/Screens/ProfileScreen';
import ChatScreen from './src/components/Screens/ChatScreen';
import OrderScreen from './src/components/Screens/OrderScreen';
import ReportsScreen from './src/components/Screens/ReportsScreen';
import EmployeesScreen from './src/components/Screens/EmployeesScreen'
import RateScreen from './src/components/Screens/RateScreen'
import AddOrderScreen from './src/components/Screens/AddOrderScreen'
import AddOrderScreen2 from './src/components/Screens/AddOrderScreen2'
import Services from './src/components/Screens/Services'
import ConfirmWorkOrderScreen from './src/components/Screens/ConfirmWorkOrderScreen'
import EditProfileScreen from './src/components/Screens/EditProfileScreen'
//icons
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
       
        <SigninScreen/>
        <SignupScreen/>
        <NavbarScreen/>
        
        <ForgetPasswordScreen/>
     
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },


});


const OrderNavigators = createStackNavigator({
  Order:{
  screen:OrderScreen,
  navigationOptions: () => ({
    header: null,
    tabBarVisible: true,
    headerMode: "screen",

    })
  },
  AddOrder:  {
      screen:AddOrderScreen,
      navigationOptions: () => ({
        header: null,
        tabBarVisible: true,
        headerMode: "screen",

      })
  },
  AddOrder2:  {
      screen:AddOrderScreen2,
      navigationOptions: () => ({
        header: null,
        tabBarVisible: true,
        headerMode: "screen",
      })
  },
  Notification: {
    screen:NotificationScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
  Services:{
    screen:Services,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
  Work: {
    screen:WorkScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
},
{
  initialRouteName: "Order"
})



const MoreNavigators = createStackNavigator({

  More: {
    screen:MoreScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
  About: {
    screen:AboutScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
  Faq: {
    screen:FaqsScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
  // Offers: {
  //   screen:OffersScreen,
  //   navigationOptions: () => ({
  //     header: null,
  //     tabBarVisible: true,
  //     headerMode: "screen",

  //     })
  // },
  Notification: {
    screen:NotificationScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },

}
)
const ProfileNavigators = createStackNavigator({
  Profile:{
    screen:ProfileScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
  Notification: {
    screen:NotificationScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },

})
const EmployeesNavigators = createStackNavigator({
  Employees:{
    screen:EmployeesScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  }
  ,
  Notification: {
    screen:NotificationScreen,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: true,
      headerMode: "screen",

      })
  },
})


function resetStack(navigation, routes) {
  if (routes.length > 1) {
    const { routeName } = routes[0];
    navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName })],
    }));
  }
}

const AppScreens = createBottomTabNavigator(
  {
    Orders:  {
      screen:OrderNavigators,
        navigationOptions: () => ({
          tabBarOnPress: ({navigation}) => {
            navigation.navigate('Order');
            // params is undefined - setting the same as HomeTab.js
          },
          tabBarIcon: ({tintColor}) => (
              <FontAwesome
                  name="calendar"
                  color={tintColor}
                  size={24}
              />
          )
          })
      },
    Organization:{
        screen:EmployeesNavigators,
        navigationOptions: () => ({
          tabBarOnPress: ({navigation}) => {
            navigation.navigate('Employees');
            // params is undefined - setting the same as HomeTab.js
          },
          tabBarVisible: true,
          headerStyle: {
            backgroundColor: '#B92F36'
          },
          tabBarIcon: ({tintColor}) => (
              <FontAwesome
                  name="users"
                  color={tintColor}
                  size={24}
              />
          )

          })

      },
    Chat: {
        screen:ChatScreen,
        navigationOptions: () => ({
              tabBarOnPress: ({navigation}) => {
                navigation.navigate('Chat');
                // params is undefined - setting the same as HomeTab.js
              },
                tabBarIcon: ({tintColor}) => (
                    <Entypo
                        name="chat"
                        color={tintColor}
                        size={24}
                    />
                )
          })
      },
      Offers: {
        screen:OffersScreen,
        navigationOptions: () => ({
          
          tabBarOnPress: ({navigation}) => {
            navigation.navigate('Offers');
            // params is undefined - setting the same as HomeTab.js
          },
          tabBarIcon: ({tintColor}) => (
            <Entypo
                name="tag"
                color={tintColor}
                size={24}
            />
        )
          })
      },
    // Reports: {
    //     screen:ReportsScreen,
    //     navigationOptions: () => ({
    //       tabBarOnPress: ({navigation}) => {
    //         navigation.navigate('Reports');
    //         // params is undefined - setting the same as HomeTab.js
    //       },
    //       header: null,
    //       tabBarVisible: true,
    //       headerMode: "screen",
    //       tabBarIcon: ({tintColor}) => (
    //           <Ionicons
    //               name="ios-analytics"
    //               color={tintColor}
    //               size={24}
    //           />
    //       )
    //       })
    //   },
    More: {
        screen:MoreNavigators,
        navigationOptions: () => ({
          tabBarOnPress: ({navigation}) => {
            navigation.navigate('More');
            // params is undefined - setting the same as HomeTab.js
          },
          tabBarIcon: ({tintColor}) => (
              <Ionicons
                  name="md-more"
                  color={tintColor}
                  size={24}
              />
          )
          })
      },
    },
    {
      tabBarOptions: {
        activeTintColor: '#e5322d',
      },

    },

);

const AppNavigator = createStackNavigator(
  {

    Signup:  {
        screen:SignupScreen,
        navigationOptions: () => ({
          title: 'Sign up',
          tabBarIcon: () => (
              <Ionicons name="ios-mail" size={15}/>
          )
        })
    },
    Rate:  {
        screen:RateScreen,
        navigationOptions: () => ({
          title: 'Sign up',
          header: null,
          tabBarVisible: true,
          headerMode: "screen",
          tabBarIcon: () => (
              <Ionicons name="ios-mail" size={15}/>
          )
        })
    },
    ForgetPassword:  {
        screen:ForgetPasswordScreen,
        navigationOptions: () => ({
          title: 'Forget Password',
          tabBarIcon: () => (
              <Ionicons name="ios-mail" style={{color:'gray'}} size={15}/>
          )
          })

    },
    Signin:  {
        screen:SigninScreen,
        navigationOptions: () => ({
          header: null,
          tabBarVisible: true,
          headerMode: "screen",
        })
    },
    EditProfileScreen: {
      screen:EditProfileScreen,
      navigationOptions: () => ({
        header: null,
        tabBarVisible: true,
        headerMode: "screen",
      })
  },

    Navbar:{
      screen:AppScreens,
      navigationOptions:() => ({
        header: null,
        }),

    },
    ConfirmWorkOrder:{
      screen:ConfirmWorkOrderScreen,
      navigationOptions:() => ({
        header: null,
        }),
    },
    Profile:{
      screen:ProfileScreen,
      navigationOptions: () => ({
        header: null,
  
        })
    },
  },
  {
    initialRouteName: "Signin"
  },
  {
    tabBarOptions: {
      showIcon: true ,

    }
  },

  );



export default createAppContainer(AppNavigator);
